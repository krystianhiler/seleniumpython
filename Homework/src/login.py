from selenium.webdriver.common.by import By


class LoginPage:
    def __init__(self, browser):
        self.browser = browser

    def load(self):
        self.browser.get("http://demo.testarena.pl/zaloguj")

    def login(self, email, password):
        email_input = self.browser.find_element(By.CSS_SELECTOR, "#email")
        email_input.send_keys(email)

        password_input = self.browser.find_element(By.CSS_SELECTOR, "#password")
        password_input.send_keys(password)

        submit_button = self.browser.find_element(By.CSS_SELECTOR, "#login")
        submit_button.click()
