from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


class AdminPanel:
    def __init__(self, browser):
        self.browser = browser

    def load(self):
        self.browser.get("http://demo.testarena.pl/administration/projects")

    def add_project(self, name, prefix):
        add_project_button = self.browser.find_element(By.CSS_SELECTOR, "a.button_link")
        add_project_button.click()

        name_input = self.browser.find_element(By.CSS_SELECTOR, "input#name")
        name_input.send_keys(name)

        prefix_input = self.browser.find_element(By.CSS_SELECTOR, "input#prefix")
        prefix_input.send_keys(prefix)

        save_button = self.browser.find_element(By.CSS_SELECTOR, "input#save")
        save_button.click()

    def search_for_project(self, project_name):
        active_menu = self.browser.find_element(By.CSS_SELECTOR, ".activeMenu")
        active_menu.click()

        search_input = self.browser.find_element(By.CSS_SELECTOR, "input#search")
        search_input.send_keys(project_name)

        search_button = self.browser.find_element(By.CSS_SELECTOR, "#j_searchButton")
        search_button.click()

    def project_exists(self, project_name):
        table_data = WebDriverWait(self.browser, 10).until(
            EC.presence_of_all_elements_located((By.CSS_SELECTOR, "td"))
        )
        for td in table_data:
            if td.text == project_name:
                return True
        return False
