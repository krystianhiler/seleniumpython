import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from faker import Faker
from webdriver_manager.chrome import ChromeDriverManager
from Homework.src.login import LoginPage
from Homework.src.admin_panel import AdminPanel


@pytest.fixture
def browser():
    browser = Chrome(executable_path=ChromeDriverManager().install())
    browser.maximize_window()
    yield browser
    browser.quit()


def test_create_project(browser):
    fake = Faker()
    name = fake.pystr(min_chars=6, max_chars=6)
    prefix = fake.pystr(min_chars=4, max_chars=4)

    browser.get("http://demo.testarena.pl/zaloguj")

    login_page = LoginPage(browser)
    login_page.load()
    login_page.login("administrator@testarena.pl", "sumXQQ72$L")

    admin_panel_page = AdminPanel(browser)
    admin_panel_page.load()
    admin_panel_page.add_project(name, prefix)

    admin_panel_page.search_for_project(name)
    assert admin_panel_page.project_exists(name)

    wait = WebDriverWait(browser, 10)
    active_menu = wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR, ".activeMenu")))
    active_menu.click()

    admin_panel_page.search_for_project(name)

    result = wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR, "td")))
    assert name in result.text
